﻿FROM golang:1.17.3

WORKDIR /go/src/gitlab/shreychen/service-a

ADD . /go/src/gitlab/shreychen/service-a

COPY ./service-a /bin/service-a

CMD ["/bin/service-a"]