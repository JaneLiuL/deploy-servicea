﻿resource "tencentcloud_eks_cluster" "tencentcloud_eks" {
  cluster_name = var.cluster_name
  k8s_version  = var.kubernetes_version
  vpc_id       = var.vpc_id
  subnet_ids = var.subnets
  cluster_desc      = var.kubernetes_description
  service_subnet_id = var.service_subnet

  enable_vpc_core_dns = true
  need_delete_cbs     = true
  tags = var.resource_tags
}