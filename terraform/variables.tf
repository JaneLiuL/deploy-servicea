﻿variable "cluster_name" {
  type        = string
  description = "Name of the Tencentcloud EKS cluster."
  default     = "test-tke"
}

variable "kubernetes_version" {
  type        = string
  description = "Desired Kubernetes version. Increase the version when desired inorder to upgrade. Downgrades are not supported by EKS."
  default     = "1.18.4"
}
variable "vpc_id" {
  type        = string
  default     = "10.0.0.0/8"
}
variable "subnets" {
  type        = list(string)
  description = <<EOF
  List of subnet IDs to associate with the Tencentcloud EKS node groups and cross-account elastic network interfaces EKS creates,
  to allow communication between your worker nodes and the Kubernetes control plane.
  The node groups require that the subnets must have the following resource tag: kubernetes.io/cluster/CLUSTER_NAME.
  Must be in at least two different availability zones.
  EOF
  default = [ "10.0.0.1/24" ]
}

variable "kubernetes_description" {
  type        = string
  description = "For create Tencentcloud eks description"
  default     = "test Tencentcloud eks cluster created by terraform"
}


variable "service_subnet" {
  type        = string
  description = "For create Tencentcloud eks service subnet"
  default = "value"
}

variable "resource_tags" {
  type        = map(string)
  description = "Key-value map of resource tags."
  default = {
    "Component" = "EKS"
    "Owner" = "JaneLiuL"
    "Expiredate" = "20211230"
  }
}

variable "secret_id" {
  type = string
  default = "my-secret-id"
}

variable "region" {
  type = string
  default = "ap-guangzhou-3"
}

variable "secret_key" {
  type = string
  default = "secret_key"
}