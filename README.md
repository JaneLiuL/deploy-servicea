这是一个为面试创建的README.md， 要求如下：

为该服务开发Dockerfile，构建容器镜像并发布到镜像仓库（地址不限）
⽤Terraform代码实现创建腾讯云TKE（实现代码即可）
开发Helm chart，将该服务部署到部署到指定的TKE集群（集群访问⽅式将以附件抄送）


其中terraform 目录是创建腾讯云TKE的terraform 代码
chart目录是将服务部署到指定的TKE集群