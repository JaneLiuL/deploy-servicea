﻿
VERSION:=v0.0.1

# IMAGE is the image name of service-a
IMAGE:=service-a:$(VERSION)

all: build

build:	
	CGO_ENABLED=0 go build -o ./service-a  gitlab/shreychen/service-a

image:
	docker build -t $(IMAGE) .

push: image	
	docker push $(IMAGE)
